/* globals app, _, angular */

app.controller('GameController', ['$scope', '$http', 'database', 'util', 'mommy', 'modal', function ($scope, $http, database, util, mommy, modal) {
  function Player(params) {
    this.leader			= params.leader;		// bool
    this.investigator		= params.investigator;		// object
    this.assets = params.assets;
    this.spells = params.spells;
    this.conditions = params.conditions;
    this.travel_tickets		= params.travel_tickets;	// array of objects
    this.spells			= params.spells;
    this.current_location	= params.current_location;
    this.clues			= params.clues;
    this.delayed = params.delayed;
    this.actions_taken		= params.actions_taken;
  }


  function findById(collection, id) {
    if (collection === undefined || id === undefined) { return null; }
    return _.find(collection, function (c) {
      return c._id === id;
    });
  }

  function InitGame() {
    database.getInvestigatorsFromDB().then(function (res) {
      $scope.available_investigators = res.data;
    });
    database.getAncientOnesFromDB().then(function (res) {
      $scope.available_ancient_ones = res.data;
    });
    database.getMythosCardsFromDB().then(function (res) {
      $scope.available_mythos_cards = res.data;
      $scope.mythos_cards = util.sortMythosCardsByColor($scope.available_mythos_cards);
    });
    database.getAssetCardsFromDB().then(function (res) {
      $scope.available_asset_cards = res.data;
    });
    database.getMapLocationsFromDB().then(function (res) {
      $scope.locations = res.data;
      $scope.available_clue_tokens = util.createClueTokens($scope.locations);
    });
    database.getGateTokensFromDB().then(function (res) {
      $scope.available_gate_tokens = res.data;
    });
    database.getMonsterTokensFromDB().then(function (res) {
      $scope.available_monster_tokens = res.data;
    });
    database.getOtherWorldCardsFromDB().then(function (res) {
      $scope.available_other_world_cards = res.data;
    });
    database.getLocationCardsFromDB().then(function (res) {
      $scope.available_location_cards = res.data;
      $scope.location_cards = util.sortLocationCardsByContinent($scope.available_location_cards);
    });
    database.getResearchCardsFromDB().then(function (res) {
      $scope.available_research_cards = res.data;
    });
    database.getConditionCardsFromDB().then(function (res) {
      $scope.available_condition_cards = res.data;
    });

    $scope.player_one = new Player({
      leader: true,
      investigator: null,
      assets: [],
      spells: [],
      conditions: [],
      travel_tickets: {
	train: 0,
	ship: 0
      },
      spells: [],
      starting_location: null,
      clues: [],
      delayed: false,
      actions_taken: {
	moves: 0,
	travel: false,
	rest: false,
	trade: false,
	prepare: false,
	acquire_assets: false,
	component_action: false,
	combat: [],
	count: 0
      }
    });
  }

  function findAssetByName(asset_name) {
    return _.find($scope.assets_deck, function (card) {
      return angular.equals(card.name, asset_name);
    });
  }
  function findConditionByName(condition_name) {
    return _.find($scope.conditions_deck, function (card) {
      return angular.equals(card.name, condition_name);
    });
  }

  function giveAssetToInvestigator(asset, investigator, stack) {
    if (_.isString(asset)) {
      asset = findAssetByName(asset);
    }

    if (stack === undefined) {
      stack = $scope.assets_deck;
    }

    stack = _.remove(stack, function (card) {
      return card._id === asset._id;
    });
    investigator.assets.push(asset);
  }

  function giveConditionToInvestigator(condition, investigator, stack) {
    if (_.isString(condition)) {
      condition = findConditionByName(condition);
    }

    if (stack === undefined) {
      stack = $scope.conditions_deck;
    }

    stack = _.remove(stack, function (card) {
      return card._id === condition._id;
    });
    investigator.conditions.push(condition);
  }

  function findLocation(location_name) {
    if (location_name === undefined || !_.isString(location_name)) { return null; }
    if (location_name.indexOf(",") > -1) {
      location_name = location_name.split(",")[0];
    }
    return _.find($scope.locations, function (location) {
      return location.name === location_name;
    });
  }

  function findConnection(loc1, loc2) {
    var ticket_type = "";
    if (loc1 === undefined || loc2 === undefined) { return ticket_type; }
    if (loc1.connections === undefined || loc2.connections == undefined) { return ticket_type; }

    var location;
    var distance = 0;

    _.each(loc1.connections, function (connection, type) {
      if (_.contains(connection, loc2.name)) {
        ticket_type = type;
	distance = 1;
	return false;
      }
    });


    if (ticket_type === "") {
      _.each(loc1.connections, function (connections, types) {
	_.each(connections, function (location_name, type) {
	  location = findLocation(location_name);
	  if (location) {
	    _.each(location.connections, function (c, t) {
	      if (_.contains(c, loc2.name)) {
		distance = 2;
		ticket_type = t;
		return false;
	      }
	    });
	  }
	});
      });
    }

    return {ticket_type: ticket_type,
	    distance: distance};
  }

  function findNearestCity(location) {
    var final_location;
    if (location === undefined) { return {}; }
    if (_.isString(location)) { location = findLocation(location); }
    if (location.type.toLowerCase().indexOf("city") > -1) {
      final_location = location;
    } else {
      var lowest = 0;

      while (!final_location) {
        var cur_location = location;
        var j = 0;
        _.each(cur_location.connections, function (connection_type) {
          _.each(connection_type, function (l) {
            cur_location = l;
            var c = findLocation(l);
            if (c) {
              if (c.type.toLowerCase().indexOf("city") > -1) {
                if (j <= lowest) {  /// TODO: should be <= and let user choose!
                  final_location = c;
                }
              }
            }
          });
        });
        j += 1;
      }
    }
    return final_location;
  }

  function canPlayerMoveTo(player, dest) {
    if (player === undefined || dest === undefined) { return false; }
    var dest_location,
      src,
      t,
      location,
      src_location,
      connection = false,
      ticket_type = "",
      ticket = true;

    if (_.isString(dest)) {
      dest_location = findLocation(dest);
    } else {
      dest_location = dest;
    }

    src = player.investigator.current_location;
    var c = findConnection(src, dest_location);

    if (!mommy.canPlayerMove(player, c)) {
      return false;
    }

    connection = (c.distance > 0);
    ticket_type = c.ticket_type;

    if (connection && ticket_type === "uncharted" && player.actions_taken.moves > 0) {
      connection = false;
    }
    
    if (((player.actions_taken.moves > 0) &&
	 (player.actions_taken.count < 2)) ||
	c.distance > 1)
    {
      if (player.travel_tickets[ticket_type] < 1) {
	ticket = false;
      }
    }

    return connection & ticket;
  }

  function moveInvestigatorTo(investigator, dest_location) {
    if (investigator === undefined 
	|| dest_location === undefined) { return; }
    var dest = findLocation(dest_location);
    if (!dest) { return; }
    investigator.current_location = dest;
  }

  function setStartingLocations() {
    var starting_location = findLocation($scope.player_one.investigator.starting_location);
    $scope.player_one.investigator.current_location = starting_location;
    starting_location.investigators.push($scope.player_one.investigator._id);
  }

  function giveInvestigatorsStartingItems() {
    _.each($scope.player_one.investigator.starting_items, function (items, type) {
      switch (type) {
      case "clue":
        var clue = $scope.clue_tokens.pop();
        $scope.player_one.clues.push(clue);
        break;
      case "assets":
        _.each(items, function (asset) {
	  giveAssetToInvestigator(asset, $scope.player_one.investigator, $scope.assets_deck);
        });
        break;
      }
    });
  }

  function dealStartingAssets() {
    var i = 0,
      card;
    for (i = 0; i < 4; i++) {
      card = $scope.assets_deck.pop();
      if (card) $scope.game.reserve.push(card);
    }
  }

  function spawnGates() {
    var gate, location;
    var i;
    var monster;
    var str = "Spawned gates at: ";
    for (i = 0; i < $scope.game.reference.spawn_gates; i++) {
      gate = $scope.gate_tokens.pop();
      $scope.game.board.gates.push(gate);
      if (gate) location = findLocation(gate.location);
      if (location) location.gate = gate;
      monster = $scope.monster_cup.pop();
      if (!monster) { return; }
      location.monsters.push(monster);
      monster.location = location;
      str += location.name + " ";
    }
    var modalInstance = modal.open(str);
  }

  function monsterSurge() {
    var locations = [];
    var monster;
    var str = "";
    _.each($scope.locations, function (location) {
      if (location.gate && location.gate.omen.indexOf($scope.game.omen) > -1) {
	locations.push(location);
      }
    });
    if (locations.length > 0) str = "Spawned monsters at: ";
    else str = "No monsters spawned";
    _.each(locations, function (location) {
      monster = $scope.monster_cup.pop();
      location.monsters.push(monster);
      monster.location = location;
      str += location.name + " - " + monster.name + "<br/>";
    });
    var modalInstance = modal.open(str);
  }

  function SetupBoard() {
    $scope.game.doom = $scope.game.ancient_one.doom;
    $scope.game.omen = "green";
    $scope.game.board = {
      gates: []
    };

    _.each($scope.available_research_cards, function (card) {
      if (card.ancient_one === $scope.game.ancient_one.name) $scope.research_cards.push(card);
    });
    $scope.research_cards = util.shuffleStack($scope.research_cards);
    $scope.assets_deck = util.shuffleStack($scope.available_asset_cards);
    $scope.conditions_deck = util.shuffleStack($scope.available_condition_cards);
    $scope.mythos_cards.green = util.shuffleStack($scope.mythos_cards.green);
    $scope.mythos_cards.red = util.shuffleStack($scope.mythos_cards.red);
    $scope.mythos_cards.blue = util.shuffleStack($scope.mythos_cards.blue);
    $scope.gate_tokens = util.shuffleStack($scope.available_gate_tokens);
    $scope.monster_cup = util.shuffleStack($scope.available_monster_tokens);
    $scope.other_world_cards = util.shuffleStack($scope.available_other_world_cards);
    $scope.clue_tokens = util.shuffleStack($scope.available_clue_tokens);
    $scope.game.reference = util.referenceCard($scope.game);
    $scope.game.mythos_deck = util.createMythosDeck($scope.game.ancient_one.mythos_deck, $scope.mythos_cards);
    $scope.player_one.investigator.improvements = {
      lore: 0,
      influence: 0,
      observation: 0,
      strength: 0,
      will: 0
    };

//    var card = _.find($scope.assets_deck, {name: ".45 Automatic"});
    //    giveAssetToInvestigator(card, $scope.player_one.investigator, $scope.assets_deck);
//    var card = _.find($scope.conditions_deck, {name: "Detained"});
//o    giveConditionToInvestigator(card, $scope.player_one.investigator, $scope.conditions_deck);
    giveInvestigatorsStartingItems();
    setStartingLocations();
//    $scope.player_one.investigator.current_location.gate = true;
    dealStartingAssets();
    spawnGates();
  }

  function DetainedEncounter(detained, investigator) {
    resolveEffects(detained.encounter, investigator);
    if (_.includes($scope.effects_queue, "discard_self")) {
      $scope.effects_queue = _.without($scope.effects_queue, "discard_self");
      investigator.conditions = _.reject(investigator.conditions, function (c) { return c._id === detained._id; });
      $scope.condition_discards.push(detained);
      $scope.msg += "Discarded Detained Condition<br/>";
    }
  }

  function spawnRandomClue() {
    var clue = $scope.clue_tokens.pop();
    var location = findLocation(clue.location);
    location.clue = true;//clue;
    $scope.msg += "Spawned clue at " + location.name + "<br/>";
  }

  function spawnClueAt(location_name) {
    var location = findLocation(location_name);
    var clue = _.find($scope.clue_tokens, function (clue) {
      return clue.name === location_name;
    });
    $scope.clue_tokens = _.without($scope.clue_tokens, clue);
    location.clue = true;//clue;
    $scope.msg += "Spawned clue at " + location.name + "<br/>";
  }

  function findMatchingGatesToOmen(omen) {
    var gates = [];
    var locations = _.reject($scope.locations, function (l) {
      return l.gate === false;
    });
    _.each(locations, function (location) {
      if (omen.toLowerCase().indexOf(location.gate.omen.toLowerCase()) > -1) gates.push(location.gate);
    });
    return gates;
  }
  function advanceOmen() {
    switch ($scope.game.omen) {
    case "green": 
      $scope.game.omen = "blue1";
      $scope.msg += "Advanced omen from green to blue<br/>";
      break;
    case "blue1":
      $scope.game.omen = "red";
      $scope.msg += "Advanced omen from blue to red<br/>";
      break;
    case "red":
      $scope.game.omen = "blue2";
      $scope.msg += "Advanced omen from red to blue<br/>";
      break;
    case "blue2":
      $scope.game.omen = "green";
      $scope.msg += "Advanced omen from blue to green<br/>";
      break;
    }
  }
  function advanceDoom() {
    $scope.msg = "";
    advanceOmen();
    var gates = findMatchingGatesToOmen($scope.game.omen);
    $scope.msg += gates.length + " gates matching current omen (" + $scope.game.omen + ")<br/>";
    if ($scope.game.doom <= gates.length) {
      $scope.msg = "Doom is 0. You LOSE!<br/>";
    } else {
      $scope.game.doom -= gates.length;
      $scope.msg += "Advanced doom by " + gates.length + "<br/>";
    }
    var modalInstance = modal.open($scope.msg);
    $scope.msg = "";
  }

  function isLocationCity(location ){
    if (location === undefined) { return false; }
    return (location.type.toLowerCase().indexOf("city") > -1);
  }

  function mythosPhase() {
    var stage = $scope.game.stage - 1;
    var card = _.first($scope.game.mythos_deck[stage]);

    _.each(card.actions, function (action, action_name) {
      if (action_name === "spawn_gates" && action) {
	spawnGates();
      }
      if (action_name === "monster_surge" && action) {
	monsterSurge();
      }
      if (action_name === "advance_doom" && action) {
	advanceDoom();
      }
      if (action_name === "spawn_clue" && action) {
        _.times($scope.game.reference.spawn_clues, function (i) {
          spawnRandomClue();
        });
      }
    });
  }


  function killMonster(monster) {
    var location = monster.location;
    location.monsters = _.without(location.monsters, monster);
    monster.location = $scope.monster_discard;
    $scope.monster_discard.push(monster);
  }

  function combat(player, monster) {
    if ($scope.game.phase !== "encounter") { return; }
    if (player === undefined || monster === undefined) { return; }
    if (!mommy.canPlayerCombatMonster(player, monster)) { $scope.msg += "Already fought " + monster.name + "<br/>"; return; }
    var modifiers = util.resolveCards(player.investigator.assets);

    // resolve monster effects
    // resolve will test
    var will = player.investigator.will;
    $scope.msg += "Core Will: " + will + "<br/>";

    will += monster.will_test;
    $scope.msg += "Monster's will test modifier: " + monster.will_test + "<br/>";

    if (modifiers.will_modifier) {
      will += modifiers.will_modifier;
      $scope.msg += "Will modifier: " + modifiers.will_modifier + "<br/>";
    }
    $scope.msg += "Final will: " + will + "<br/>";

    var roll = util.rollDice(will);
    var will_test = util.testRoll(roll);
    var color = (will_test.pass ? 'green' : 'red');
    $scope.msg += "Will test passed: <span style='color:" + color + "'>" + will_test.pass + "</span><br/>";

    var sanity_loss = monster.horror - will_test.successes;
    if (sanity_loss > 0) {
      $scope.msg += "Sanity loss: " + sanity_loss + "<br/>";
      player.investigator.sanity -= sanity_loss;
      if (player.investigator.sanity <= 0) {
	// investigator is insane
      }
    }

    var strength = player.investigator.strength;
    $scope.msg += "Core Strength: " + strength + "<br/>";

    strength += monster.strength_test;
    $scope.msg += "Monster's strength test modifier: " + monster.strength_test + "<br/>";

    if (modifiers.strength_modifier) {
      strength += modifiers.strength_modifier;
      $scope.msg += "Strength modifier from assets: " + modifiers.strength_modifier + "<br/>";
    }
    $scope.msg += "Final strength: " + strength + "<br/>";

    roll = util.rollDice(strength);
    var strength_test = util.testRoll(roll);
    color = (strength_test.pass ? 'green' : 'red');
    $scope.msg += "Strength test passed: <span style='color:" + color + "'>" + strength_test.pass + "</span><br/>";

    var health_loss = monster.damage - strength_test.successes;
    if ((health_loss - modifiers.damage) > modifiers.minimum_reduce) {
      health_loss += modifiers.damage;
      $scope.msg += "Damage modifiers from assets: " + modifiers.damage + "<br/>";
    }

    if (health_loss > 0) {
      $scope.msg += "Health loss: " + health_loss + "<br/>";
      player.investigator.health -= health_loss;
      if (player.investigator.health <= 0) {
	// investigator is dead
      }
    }
    if (strength_test.successes > 0) {
      monster.toughness -= strength_test.successes;
      $scope.msg += "Monster's loss of health: " + strength_test.successes + "<br/>";
      if (monster.toughness <= 0) {	
	// monster is dead
	killMonster(monster);
	$scope.msg += "Monster is dead! tada!<br/>";
      }
    }
  }

  function closeGate(gate) {
    var location = findLocation(gate.location);
    location.gate = false;
    $scope.game.board.gates = _.without($scope.game.board.gates, gate);
    $scope.gates_discard.push(gate);
  }

  function canPickOption(effect) {
    var gain, lose;
    if (effect.lose) {
      lose = effect.lose;
      if (lose.type === "clue") {
        if ($scope.player_one.clues.length < lose.amount) return false;
      }
    } else if (effect.gain) {
      gain = effect.gain;
      if (gain.type === "asset") {
        if (gain.from === "deck") {
          if ($scope.assets_deck.length < gain.amount) return false;
        } else if (gain.from === "reserve") {
          if ($scope.game.reserve.length < gain.amount) return false;
        }
      }
    }
    return true;
  }

  function resolveEffects(effects, investigator) {
    var modalInstance;

    if (effects.lose_sanity) {
      console.log("!DEPRECATED! please use lose.amount, lose.attr");
    }
    if (effects.close_gate) {
      closeGate(investigator.current_location.gate);
      $scope.msg += "Gate closed: " + investigator.current_location.name + "<br/>";
    }
    if (effects.improve) {
      var skill = effects.improve;
      if (investigator.improvements[skill] < 2) investigator.improvements[skill] += 1;
      $scope.msg += "Improved " + skill + "<br/>";
    }
    if (effects.become) {
      investigator[effects.become] = true;
      $scope.msg += "Became " + effects.become + "<br/>";
    }
    if (effects.move_omen) {
      options = [{desc: "Green", enabled: true, index: "green"},
                 {desc: "Blue 1", enabled: true, index: "blue1"},
                 {desc: "Red", enabled: true, index: "red"},
                 {desc: "Blue 2", enabled: true, index: "blue2"}];
      modalInstance = modal.OrModal(options, function (o) {
        if (_.findWhere(options, {index: o})) { $scope.game.omen = o; }
        if (effects.move_omen.advance_doom) advanceDoom();
      });
    }
    if (effects.discard_self) {
      $scope.effects_queue.push("discard_self");
    }
    if (effects.gain) {  // make this more meta!!!
      var gain = effects.gain;
      var asset, deck, n, clue, card;
      if (gain.amount === undefined) { gain.amount = 1; }
      _.times(gain.amount, function () {
        if (gain.type === "condition") {
          card = _.findWhere($scope.conditions_deck, {name: gain.name});
          if (card) {
            giveConditionToInvestigator(card, investigator, $scope.conditions_deck);
            $scope.msg += "Gained " + card.name + " condition<br/>";
          }
        }
        if (gain.from === "reserve") {
          deck = $scope.game.reserve;
        } else if (gain.from === "deck") {
          deck = gain.type + "_cards";
          deck = $scope[deck];
        } else if (gain.at === "random") {
          if (gain.type === "clue") {
            deck = $scope.clue_tokens;
            clue = deck.pop();
            $scope.player_one.clues.push(clue);
            $scope.msg += "Gained clue<br/>";
          }
        }
        if (gain.type === "asset") {
          asset = deck.pop();
          giveAssetToInvestigator(asset, $scope.player_one.investigator, deck);
          $scope.msg += "Drew card from " + gain.from + ": " + asset.name + "<br/>";
        }
      });
    }
    if (effects.lose) {
      var lose = effects.lose;
      var amount;
      amount = lose.amount;
      var attr = lose.attr;
      if (amount === undefined) amount = 1;
      if (lose.token) {
        if (lose.token === "clue") {
          clue = $scope.player_one.clues.pop();
          $scope.clue_discards.push(clue);
          $scope.msg += "Lost " + amount + " clues</br>";
        }
      } else {
        if (attr === "health") {
          investigator.health -= amount;
          $scope.msg += "Lost " + amount + " health<br/>";
        }
        if (attr === "sanity") {
          investigator.sanity -= amount;
          $scope.msg += "Lost sanity: " + amount + "<br/>";
        }
      }
    }
    if (effects.spawn) {
      if (effects.spawn.token === "clue") {
        _.times(effects.spawn.amount, function () {
          if (effects.spawn.at === "random") {
            spawnRandomClue();
          } else {
            spawnClueAt(effects.spawn.at);
          }
        });
      }
      $scope.msg += "Spawned " + effects.spawn.amount + " " + effects.spawn.token + " total<br/>";
    }// yeah! meta!
    if (effects.or) {
      var options = [{desc: util.formSentence(effects.or[0]), enabled: canPickOption(effects.or[0]), index: 0},
                     {desc: util.formSentence(effects.or[1]), enabled: canPickOption(effects.or[1]), index: 1}];
      modalInstance = modal.OrModal(options, function (o) {
	var choice = parseInt(o, 10);
	var selection = effects.or[choice];
	resolveEffects(selection, investigator);
      });
    }
    if (effects.test) {
      resolveTest(effects.test, investigator);
      modalInstance = modal.open($scope.msg);
    }
  }

  function resolveTest(test, investigator) {
    var skill = test.skill;
    var modifier = test.modifier;
    var s = investigator[skill] + investigator.improvements[skill];
    if ((s > 1 && modifier < 0) || modifier > 0) {
      s += modifier;
    }
    var rolls = util.rollDice(s);
    var rollTest = util.testRoll(rolls);
    if (rollTest.pass) {
      $scope.msg += "Passed " + skill + " test!<br/>";
      if (test.pass) resolveEffects(test.pass, investigator);
    } else {
      $scope.msg += "Lost " + skill + " test...<br/>";
      if (test.fail) resolveEffects(test.fail, investigator);
    }
    return rollTest;
  }
  function resolveComplexEncounter(card, investigator) {
    var test = card.test;
    var skill = test.skill;
    var modifier = card.test.modifier;
    var s = investigator[skill] + investigator.improvements[skill];
    var modalInstance;
    if ((s > 1 && modifier < 0) || modifier > 0) {
      s += modifier;
    }

    var rolls = util.rollDice(s);
    var rollTest = util.testRoll(rolls);

    if (rollTest.pass) {
      $scope.msg += "Passed " + skill + " test.<br/>";
      var pass = test.pass;
      resolveEffects(pass, investigator);

      if (pass.test) {
        test = pass.test;
        resolveTest(test, investigator);
      }
    } else {
      $scope.msg += "Failed " + skill + " test...<br/>";
      var fail = test.fail;
      if (fail.or) {
        var options = [{desc: util.formSentence(fail.or[0]), enabled: canPickOption(fail.or[0]), index: 0},
                       {desc: util.formSentence(fail.or[1]), enabled: canPickOption(fail.or[1]), index: 1}];
	modalInstance = modal.OrModal(options, function (o) {
	  var choice = parseInt(o, 10);
	  var selection = fail.or[choice];
	  resolveEffects(selection, investigator);
	});
      }
    }
  }

  function investigatorDefeated(player) {
      advanceDoom();
      var city = findNearestCity(player.investigator.current_location);
      var investigator = player.investigator;
      investigator.current_location = city;
      investigator.defeated = true;
      // TODO: collect possessions - discard everything
      // TODO: allow player to choose new investigator
      $scope.game.defeated.push(investigator);
      player.investigator = null;
      chooseNewInvestigator(player);
  }
  function chooseNewInvestigator(player) {
    var options = [];
    var available_investigators = _.clone($scope.available_investigators, true);
    _.each($scope.game.defeated, function (defeated) {  // remove all defeated
      available_investigators = _.reject(available_investigators, function (inv) {
        return inv.name === defeated.name;
      });
    });
    _.each(available_investigators, function (investigator, index) {
      options.push({desc: investigator.name, enabled: true, index: index});
    });
    var modalInstance = modal.OrModal(options, function (o) {
      var choice = parseInt(o, 10);
      player.investigator = available_investigators[o];
      giveInvestigatorsStartingItems();
      setStartingLocations();
    });

  }
  /** Scope Methods **/

  $scope.isSetupPhase = function () {
    return $scope.game.phase === "setup";
  };

  $scope.isSelectedAncientOne = function (ancient_one) {
    return angular.equals($scope.game.ancient_one, ancient_one);
  };

  $scope.isSelectedInvestigator = function (investigator) {
    return angular.equals($scope.player_one.investigator, investigator);
  };

  $scope.startGame = function () {
    if (_.isEmpty($scope.game.ancient_one)) {
      var l = $scope.available_ancient_ones.length;
      var rand = Math.floor(Math.random() * l);
      $scope.game.ancient_one = $scope.available_ancient_ones[rand];
    }
    if (_.isEmpty($scope.player_one.investigator)) {
      var l = $scope.available_investigators.length;
      var rand = Math.floor(Math.random() * l);
      $scope.player_one.investigator = $scope.available_investigators[rand];
    }
    $scope.game.phase = "action";
    SetupBoard();
  };

  $scope.getLocationInfoStyle = function (location) {
    if (location === undefined) { return {}; }
    if (_.isString(location)) location = findLocation(location);
    var coords = location.coords.split(',');
    var left = coords[0];
    var top = coords[1];
    var style = {
      "position": "absolute",
      "background-color": "white",
      "left": left + "px",
      "top": top + "px"
    };
    return style;
  };
  $scope.initials = function (str) {
    var s = str.split(" ");
    return s[0][0] + "" + s[1][0];
  };

  $scope.locationCoords = function (location) {
    if (location === undefined) { return ""; }
    var c = location.coords;
    var type = location.type;
    var r = 23;
    if (type.toLowerCase().indexOf("major") > -1) {
      r = 80;
    }
    return c + "," + r;
  };

  $scope.clickMapAt = function (location_name) {
    if ($scope.game.phase === "action" && !$scope.player_one.investigator.delayed) {
      if (canPlayerMoveTo($scope.player_one, location_name)) {
        var loc1 = $scope.player_one.investigator.current_location;
        var loc2 = findLocation(location_name);
        var connection = findConnection(loc1, loc2);
        $scope.player_one = util.playerMove($scope.player_one, connection);
        moveInvestigatorTo($scope.player_one.investigator, location_name);
      }
    }
  };

  $scope.clickCardInReserve = function (card) {
    if ($scope.game.phase === "action" && !$scope.player_one.investigator.delayed) {
      var player = $scope.player_one;
      if (!mommy.canPlayerTakeCardFromReserve(player)) { return; }
      var roll = util.rollDice(player.investigator.influence);
      var test = util.testRoll(roll);
      var modalInstance;
      if (test.pass && test.successes >= card.value) {
	giveAssetToInvestigator(card, player.investigator, $scope.game.reserve);
	  str = "Rolled influence.<br/>Passed: " + test.pass + "<br/>Successes: " + test.successes + "<br/>Asset value: " + card.value + "<br/>Acquired: " + (test.successes >= card.value);
	modalInstance = modal.open(str);
      } else {
	if (player.clues.length > 0) {
	  var amount = 0;
	  modalInstance = modal.SpendCluesToRerollModal($scope.player_one.clues, function (a) {
            amount = parseInt(a, 10);
            _.times(amount, function () {
	      var card = $scope.player_one.clues.pop();
              $scope.clue_discards.push(card);
            });
	    roll = util.reroll(roll, amount);
	    test = util.testRoll(roll);
	    if (test.pass && test.pass.successes >= card.value) {
	      giveAssetToInvestigator(card, player.investigator, $scope.game.reserve);
	      str = "Rolled influence.<br/>Passed: " + test.pass + "<br/>Successes: " + test.successes + "<br/>Asset value: " + card.value + "<br/>Acquired: " + (test.successes >= card.value);
	    } else {
	      str = "Not enough rolls passed<br/>Did not aquire card<br/>";
	    }
	    modalInstance = modal.open(str);
	  });
	} else {
	  str = "Rolled influence.<br/>Passed: " + test.pass + "<br/>Successes: " + test.successes + "<br/>Asset value: " + card.value + "<br/>Acquired: " + (test.successes >= card.value);
	  modalInstance = modal.open(str);
	}
      }
      player.actions_taken.acquire_assets = true;
      player.actions_taken.count += 1;
    }
  }

  $scope.clickGetTravelTicketAction = function (ticket_type, player) {
    if ($scope.game.phase !== "action" || $scope.player_one.investigator.delayed) { return; }
    if (player === undefined) player = $scope.player_one;
    if (player.actions_taken.prepare || player.actions_taken.count > 1) { return; }
    var location = player.investigator.current_location;
    if (isLocationCity(location)) {
      if (location.connections[ticket_type].length > 0) {
	player.travel_tickets[ticket_type] += 1;
	player.actions_taken.count += 1;
	player.actions_taken.prepare = true;
      }
    }
  };

  $scope.clickRestAction = function (player) {
    if (player === undefined) player = $scope.player_one;
    if (!mommy.canPlayerRest(player, $scope.game.phase)) { return; }
    var investigator = player.investigator;
    if (investigator.health < investigator.max_health)
      investigator.health += 1;
    if (investigator.sanity < investigator.max_sanity)
      investigator.sanity += 1;
    player.actions_taken.count += 1;
    player.actions_taken.rest = true;
  };

  $scope.clickEndPhase = function () {
    if ($scope.game.phase === "action") {
      $scope.game.phase = "encounter";
      $scope.player_one.investigator.delayed = false; // lift delay - move to function?
    } else if ($scope.game.phase === "encounter") {
      $scope.game.phase = "mythos";      
      mythosPhase();
    } else if ($scope.game.phase === "mythos") {
      $scope.game.phase = "action";
      $scope.player_one.actions_taken = util.resetActionsTaken();
    }
  };

  $scope.clickCombatEncounter = function () {
    var player = $scope.player_one;
    var investigator = player.investigator;
    var detained = _.findWhere(investigator.conditions, {name:"Detained"});
    $scope.msg = "";
    if (detained) {
      DetainedEncounter(detained, investigator);
    } else {
      var location = investigator.current_location;
      var monster = location.monsters[0];
      _.each(location.monsters, function (monster) {
        combat($scope.player_one, monster);
        player.actions_taken.combat.push(monster);
      });
      var modalInstance = modal.open($scope.msg);
    }
  };

  $scope.clickGateEncounter = function () {
    if ($scope.game.phase !== "encounter") { return; }
    var player = $scope.player_one;
    var investigator = player.investigator;
    var location = investigator.current_location;
//    if (location.monsters.length <= 0) { return; }
    if (!location.gate) { return; }
    var card = _.first($scope.other_world_cards);//$scope.other_world_cards.shift();
    $scope.msg = "";
    resolveComplexEncounter(card, investigator);
    var modalInstance = modal.open($scope.msg);
  };

  $scope.clickLocationEncounter = function () {
    if ($scope.game.phase !== "encounter") { return; }
    var player = $scope.player_one;
    var investigator = player.investigator;
    var location = investigator.current_location;
    //if (location.monsters.length <= 0) { return; }
    //var continent = location.continent.toLowerCase();
    var continent = "general";
    var card = $scope.location_cards[continent].pop();
    $scope.location_card_discards[continent].push(card);
    var location_type = location.type.toLowerCase().replace('major ', '');
    var test = card[location_type].test;
    var effects = card[location_type].effects;
    $scope.msg = "";
    if (test) resolveTest(test, investigator);
    if (effects) resolveEffects(effects, investigator);
    var modalInstance = modal.open($scope.msg);
  };

  $scope.clickResearchEncounter = function () {
    if ($scope.game.phase !== "encounter") { return; }
    var player = $scope.player_one;
    var investigator = player.investigator;
    var location = investigator.current_location;
    if (!location.clue) { return; }
    var clue = location.clue;
    var location_type = location.type.toLowerCase().replace('major ', '');
    var card = $scope.research_cards.pop();
    $scope.research_discards.push(card);
    var test = card[location_type].test;
    var effects = card[location_type].effects;
    $scope.msg = "";
    if (test) {
      var rollTest = resolveTest(test, investigator);
      if (rollTest.pass) {
        location.clue = false;
        player.clues.push(clue);
      }
    }
    if (effects) {
      resolveEffect(effects, investigator);
    }
    var modalInstance = modal.open($scope.msg);
  };

  $scope.getDefeatedInvestigatorsAt = function (location) {
    var invs = [];
    _.each($scope.game.defeated, function (d) {
      if (d.current_location.name === location.name) invs.push(d);
    });
    return invs;
  };

  $scope.defeatedInvestigatorsAt = function (location) {
    var d = $scope.getDefeatedInvestigatorsAt(location);
    if (d.length > 0) { return true; }
    return false;
  };

  $scope.$watch('game.phase', function (n, o) {
    if (n === "action") {
      $scope.showReserveCards = true;
      $scope.showTravelTickets = true;
    } else {
      $scope.showReserveCards = false;
      $scope.showTravelTickets = false;
    }
  }, true);

  $scope.$watch('clue_tokens', function (n, o) {
    if (n.length === 0) {
      $scope.clue_tokens = util.shuffleStack($scope.clue_discards);
      $scope.clue_discards = [];
    }
  }, true);

  $scope.$watch('location_cards', function (n, o) {
    _.each(n, function (arr, continent) {
      if (arr.length === 0) {
        $scope.location_cards[continent] = util.shuffleStack($scope.location_card_discards[continent]);
      }
    });
  }, true);

  $scope.$watch('research_cards', function (n, o) {
    if (n.length === 0) {
      $scope.research_cards = util.shuffleStack($scope.research_discards);
      $scope.research_discards = [];
    }
  }, true);

  $scope.$watch('game.reserve', function (n, o) {
    if (n.length < 4) {
      var card;
      _.times(4 - n.length, function () {
        card = $scope.assets_deck.pop();
        if (card) $scope.game.reserve.push(card);
      });
    }
  }, true);

  $scope.$watch('player_one.investigator.sanity', function (n, o) {
    if (n <= 0) {
      investigatorDefeated($scope.player_one);
    }
  }, true);

  $scope.$watch('player_one.investigator.health', function (n, o) {
    if (n <= 0) {
      investigatorDefeated($scope.player_one);
    }
  }, true);

  /** Init **/
  $scope.init = function () {
    $scope.game = {
      omen: "",
      doom: 0,
      ancient_one: null,
      mythos_deck: [],
      phase: "setup",
      stage: 1,
      players: [],
      reserve: [],
      reference: {},
      gates: [],
      defeated: []
    };

    $scope.available_investigators = [];
    $scope.available_ancient_ones = [];
    $scope.available_mythos_cards = [];
    $scope.available_asset_cards = [];
    $scope.available_gate_tokens = [];
    $scope.available_monster_tokens = [];
    $scope.available_other_world_cards = [];
    $scope.available_location_cards = [];
    $scope.available_clue_tokens = [];
    $scope.available_research_cards = [];

    $scope.locations = [];
    $scope.monster_cup = [];
    $scope.clue_tokens = [];
    $scope.assets_deck = [];
    $scope.mythos_cards = {};
    $scope.gate_tokens = [];
    $scope.other_world_cards = [];
    $scope.research_cards = [];

    $scope.monster_discard = [];
    $scope.gates_discard = [];
    $scope.clue_discards = [];
    $scope.research_discards = [];
    $scope.location_card_discards = {
      america: [],
      europe: [],
      asia_australia: [],
      general: []
    };
    $scope.condition_discards = [];

    $scope.effects_queue = [];

    $scope.showPlayerInfoPanel = true;
    $scope.showReferenceCard = false;
    $scope.showReserveCards = false;
    $scope.showActionsTaken = false;
    $scope.showTravelTickets = false;

    InitGame();
  };
}]);
