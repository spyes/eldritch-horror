app.controller('OrModalController', ['$scope', '$modalInstance', 'options', function ($scope, $modalInstance, options) {
  $scope.choice = 0;
  $scope.options = options;

  $scope.choose = function (c) {
    $scope.choice = c;
  };

  $scope.ok = function () {
    var c = _.find($scope.options, function (o) { return o.index === $scope.choice; });
    if (c && c.enabled) {
      $modalInstance.close($scope.choice);
    }
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}]);
