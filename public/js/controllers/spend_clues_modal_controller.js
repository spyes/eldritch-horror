app.controller("SpendCluesModalController", ['$scope', '$modalInstance', 'clues', function ($scope, $modalInstance, clues) {
  $scope.choice = 0;
  $scope.clues = clues;

  $scope.ok = function () {
    $modalInstance.close($scope.choice);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.cluesArr = function () {
    return new Array(clues+1); // allows choosing '0'
  }
}]);